import xml.dom.minidom

class Element:
    def __init__(self, nombre, dic):
        self.nombre = nombre
        self.dic = dic

    def name(self):
        return self.nombre

    def attrs(self):
        return self.dic

class SMIL:
    def __init__(self, path):
        self.path = path
    
    def elements(self):
        document = xml.dom.minidom.parse(self.path)
        elements = document.getElementsByTagName('*')
        lista = []
       
        for elemento in elements:
            atributos = elemento.attributes
            dicc_atributos = {}

            for i in range(atributos.length):
                nombre = atributos.item(i).name
                valor= atributos.item(i).value
                dicc_atributos[nombre] = valor
            lista.append(Element(elemento.tagName, dicc_atributos))

        return lista

if __name__ == "__main__":
    karaoke = SMIL('karaoke.smil')
    elementos = karaoke.elements()