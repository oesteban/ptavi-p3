import smil
import sys
import os

def main():
    if len(sys.argv) > 1:
        path = sys.argv[1]
        document = smil.SMIL(path)
        
        file = open("file.json", "w")
        file.write("[" + os.linesep)

        for elemento in document.elements():
            file.write("    {" + os.linesep)
            file.write("        \"name\": " + "\"" + elemento.name() + "\"," + os.linesep)
            file.write("        \"attrs\": " + str(elemento.attrs()) + os.linesep)
            file.write("    }," + os.linesep)
        file.write("]")
        file.close()



if __name__ == "__main__":
    main()