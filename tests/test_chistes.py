#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
from io import StringIO
import os
import unittest

from xml.sax import make_parser
import chistes

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')

result_text = """Calificación: malo.
 Pregunta: ¿Por qué los buzos se tiran de espaldas?
 Respuesta: Si se tiraran de frente, caerían en el barco.
Calificación: malisimo.
 Pregunta: ¿Qué es lo mejor de ser piloto?
 Respuesta: Que se te pasa el tiempo volando.
Calificación: regular.
 Pregunta: ¿Qué le dice una pared a otra pared?
 Respuesta: Te veo en la esquina.
"""


class TestChistes(unittest.TestCase):

    def test_main(self):
        stdout = StringIO()
        with contextlib.redirect_stdout(stdout):
            os.chdir(parent_dir)
            chistes.main()
        output = stdout.getvalue()
        self.assertEqual(output, result_text)

if __name__ == '__main__':
    unittest.main(module=__name__, buffer=True, exit=False)
