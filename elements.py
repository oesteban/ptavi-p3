import smil
import sys


def main(path):
    document = smil.SMIL(path)
    for elemento in document.elements():
        print(elemento.name())

if __name__ == "__main__":
    try:
        main(sys.argv[1])
    except:
        print("Usage: python3 elements.py <file>")