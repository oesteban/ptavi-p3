#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Simple program to parse a chistes XML file"""

import xml.dom.minidom



def main(path):
    calificacion = ['buenisimo', 'bueno', 'regular', 'malo', 'malisimo']
    document = xml.dom.minidom.parse(path)
    jokes = document.getElementsByTagName('chiste')
    jokesArray = []
    
    #Excepcion humor
    raiz = document.getElementsByTagName('humor')
    try:
        existe = raiz[0].firstChild.nodeValue.strip()
    except Exception:
        print("ROOT ELEMENT IS NOT HUMOR")


    for joke in jokes:
        score = joke.getAttribute('calificacion')
        questions = joke.getElementsByTagName('pregunta')
        question = questions[0].firstChild.nodeValue.strip()
        answers = joke.getElementsByTagName('respuesta')
        answer = answers[0].firstChild.nodeValue.strip()
        jokesArray.append((score, question, answer))

    for calif in calificacion:
        for joke in jokesArray:
            joke_calif = joke[0]
            if joke_calif == calif:
                print(f"Calificación: {calif}.")
                print(f" Respuesta: {(joke[2])}")
                print(f" Pregunta: {(joke[1])}\n")


if __name__ == "__main__":
    main('tests/chistes8.xml')