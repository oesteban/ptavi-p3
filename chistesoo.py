#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Simple program to parse a chistes XML file"""

import xml.dom.minidom


def main(path):
    obj = Humor(path)
    for joke in obj.jokes():
        print(f" Calificación: {joke[0]}.")
        print(f" Respuesta: {(joke[2])}")
        print(f" Pregunta: {(joke[1])}\n")
        
        


class Humor:
    def __init__(self, path):
        self.path = path

    def jokes(self):
        calificaciones = ['buenisimo', 'bueno', 'regular', 'malo', 'malisimo']
        document = xml.dom.minidom.parse(self.path)
        jokes = document.getElementsByTagName('chiste')
        jokesArray = []
        jokesArrayOrdenado = []

         #Excepcion humor
        raiz = document.getElementsByTagName('humor')
        try:
            existe = raiz[0].firstChild.nodeValue.strip()
        except Exception:
            print("ROOT ELEMENT IS NOT HUMOR")

        for joke in jokes:
            score = joke.getAttribute('calificacion')
            questions = joke.getElementsByTagName('pregunta')
            question = questions[0].firstChild.nodeValue.strip()
            answers = joke.getElementsByTagName('respuesta')
            answer = answers[0].firstChild.nodeValue.strip()
            jokesArray.append((score, question, answer))

        for calif in calificaciones: #ordenamos la lista anterior
            for joke in jokesArray:
                joke_calif = joke[0]
                if joke_calif == calif:
                    jokesArrayOrdenado.append((joke_calif, joke[1], joke[2]))

        return jokesArrayOrdenado


if __name__ == "__main__":
    main('tests/chistes8.xml')
